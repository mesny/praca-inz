/**
 * @file memory_client.c
 * @brief Zawiera kod klienta pamięci współdzielonej
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <mqueue.h>

#include "memory_shared.h"
#include "./../includes/signal_local.h"
#include "./../includes/trim_local.h"

void parse_args(int argc, char *argv[], char *data);
void token_read(char *token, int *server_pid, int *server_cur);

/**
 * Zmienna sterująca decydująca o tym, czy program powinien zakończyć działanie.
 */
char quit = 0;

/**
 * Funkcja główna programu, przykładowa implementacja klienta pamięci współdzielonej.
 * @param argc Ilość przekazanych argumentów
 * @param argv Wartości argumentów
 * @return int
 */
int main (int argc, char *argv[])
{
    char data[MESSAGE_SIZE];
    parse_args(argc, argv, data);

    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = QUEUE_MAX_MESSAGES;
    attr.mq_msgsize = QUEUE_MESSAGE_SIZE;
    attr.mq_curmsgs = 0;

    pid_t pid = getpid();
    mqd_t qfd_server, qfd_client;

    struct memory_req *memory_ptr_req;
    struct memory_res *memory_ptr_res;
    struct memory_inf *memory_ptr_inf;
    int fd_shm_req, fd_shm_res, fd_shm_inf;
    memory_create_req(&fd_shm_req,&memory_ptr_req);
    memory_create_res(&fd_shm_res, &memory_ptr_res);
    memory_create_inf(&fd_shm_inf, &memory_ptr_inf);

    int signal_fd;
    signal_attach(&signal_fd);

    char q_client_name[32];
    sprintf(q_client_name, "%s-%d", CURSOR_CLIENT, pid);
    trim_inplace(q_client_name,"\0");

    /* Kursor, czyli adres w pamięci, który jest przyznany klientowi dla nieblokującego zapisu / odczytu,
     * w celu uniknięcia konieczności użycia semafor-ów / mutex-ów.
     * Kolejność:
     * 1. Dynamiczne utworzenie kolejki w queue_client, nazwę tej kolejki pchamy to queue_broker
     * 2. queue_broker odczytuje nazwę kolejki, inkrementuje kursor, i wysyła wartość kursora przez kolejkę do queue_client
     * 3. queue_client odczytuje wartość kursora z kolejki i wykonuje żądaną operacje na pamięci, w miejscu wskazanym przez kursor
     * 4. queue_client, po zakończeniu działania wysyła sygnał do queue_broker
     * 5. queue_broker odbiera sygnał i odczytuje wybrany obszar pamięci zapisany przez queue_client
     */
    if ((qfd_client = mq_open(q_client_name, O_RDONLY | O_CREAT, QUEUE_PERMISSIONS, &attr)) == -1)
        report_and_exit("mq_open qfd_client");
    if ((qfd_server = mq_open(CURSOR_SERVER, O_WRONLY)) == -1)
        report_and_exit("mq_open qfd_server");
    if (mq_send(qfd_server, q_client_name, strlen(q_client_name) + 1, 0) == -1)
        report_and_exit("mq_send qfd_server");

    int server_pid, server_cur;
    char token[24];
    if (mq_receive(qfd_client, token, QUEUE_MESSAGE_SIZE, NULL) == -1)
        report_and_exit("mq_receive qfd_client");
    token_read(token, &server_pid, &server_cur);

    printf("[+] cursor: %d, pid: %d\n", server_cur, server_pid);
    printf("[+] user data: %s\n", data);

    strncpy(memory_ptr_req->req[server_cur], data, MESSAGE_SIZE);
    signal_notify(server_pid, SIGUSR1, server_cur);

    if (mq_close(qfd_server) == -1)
        report_and_exit("mq_close qfd_server");

}

/**
 * Funkcja analizuje argumenty przekazane do programu i ustawia
 * @param argc Ile argumentów było przekazane z command line
 * @param argv Tablica przechowująca wartości argumentów przekazane z command line
 * @param com Zmienna oznaczająca pożądaną operację - get, set, add,
 * @param key Klucz do danych, które są przedmiotem operacji
 * @param val Wartość - używana w przypadku operacji **set** lub **add**
 */
void parse_args(int argc, char *argv[], char *data)
{
    if (argc != 2) {
        printf("[e] invalid args\n");
        exit(EXIT_FAILURE);
    }

    memset(data, 0, sizeof (*data));
    strcpy(data, argv[1]);
}

/**
 * Funkcja dekodująca dane przekazane od serwera i odczytująca z nich PID serwera
 * i ID kursora zasobu pamięci współdzielonej
 * @param token Surowa wartość przekazana przez serwer
 * @param server_pid Odczytany PID serwera
 * @param server_cur Odczytana wartość kursora zasobu pamięci współdzielonej
 */
void token_read(char *token, int *server_pid, int *server_cur)
{
    char token_pid[8];
    char *delim = " ";
    char *token_num = strchr(token, *delim);

    memset(token_pid, 0, sizeof(token_pid));
    if (token_num == NULL)
        report_and_exit("Invalid token format");

    strncpy(token_pid, token, token_num - token);
    *server_pid = atoi(token_pid);
    *server_cur = atoi(token_num+1);
}


