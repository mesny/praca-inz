/**
 * @file memory_shared.h
 * @brief Plik zawiera stałe i funkcje biblioteczne, wspólne dla programów klienta i brokera pamięci współdzielonej
 */

#ifndef PRACA_INZ_MEMORY_SHARED_H
#define PRACA_INZ_MEMORY_SHARED_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <mqueue.h>
#include <error.h>
#include <errno.h>
#include <sys/mman.h>

#define MEMORY_REQ "/wsh-memory-req"
#define MEMORY_RES "/wsh-memory-res"
#define MEMORY_INF "/wsh-memory-inf"
#define MEMORY_CUR "/wsh-memory-cur"

/**
 * Uprawnienia do zasobów pamięci
 */
#define QUEUE_PERMISSIONS 0660

/**
 * Ile maksymalnie, oczekujących, nieodebranych komunikatów może istnieć w kolejce
 */
#define QUEUE_MAX_MESSAGES 10

/**
 * Maksymalna wielkość komunikatu przechowywanego w kolejce, w bajtach
 */
#define QUEUE_MESSAGE_SIZE 256

/**
 * Nazwa głównej kolejki
 */
#define CURSOR_SERVER "/wsh-cursor-server-queue\0"

/**
 * Nazwa kolejki
 */
#define CURSOR_CLIENT "/wsh-cursor-client-queue"

/**
 * Maksymalna wartość kursora w bajtach
 */
#define MAX_CURSOR 128

/**
 * Maksymalna długość wiadomości w pamięci współdzielnej, w bajtach
 */
#define MESSAGE_SIZE 1024

/**
 * Struktura pamięci współdzielonej do przechowywania żądań
 */
struct memory_req {
    char req[MAX_CURSOR][MESSAGE_SIZE];
};

/**
 * Struktura pamięci współdzielonej do przechowywania odpowiedzi
 */
struct memory_res {
    char res[MAX_CURSOR][MESSAGE_SIZE];
};

/**
 * Struktura pamięci współdzielonej do przechowywania statystyk
 */
struct memory_inf {
    int inf[MAX_CURSOR];
};

/**
 * Struktura pamięci współdzielonej do przechowywania wartości kursora
 */
struct memory_cur {
    unsigned int cursor;
};

/**
 * Funkcja wyświetla komunikat o błędzie i kończy program z kodem EXIT_FAILURE.
 * @param msg Wskaźnik do tekstu komunikatu, który chcemy wysłać na STDOUT
 */
void
report_and_exit (char *msg)
{
    perror (msg);
    exit (errno);
}

/**
 * Funkcja tworzy współdzielona zasób pamięci który będzie przechowywał ciała żądań
 * @param fd_shm_res Uchwyt do operacji zarządzania pamięcią
 * @param memory_ptr_res Wskaźnik do utworzonej pamięci
 */
void memory_create_req(int *fd_shm_req, struct memory_req **memory_ptr_req)
{
    if ((*fd_shm_req = shm_open (MEMORY_REQ, O_RDWR | O_CREAT, 0660)) == -1)
        report_and_exit("shm_open req");

    if (ftruncate(*fd_shm_req, sizeof (struct memory_req)) == -1)
        report_and_exit("ftruncate req");

    *memory_ptr_req = mmap(NULL, sizeof(struct memory_req), PROT_READ | PROT_WRITE, MAP_SHARED, *fd_shm_req, 0);
    if (*memory_ptr_req == MAP_FAILED) {
        report_and_exit("mmap req");
    }
}

/**
 * Funkcja tworzy współdzielona zasób pamięci który będzie przechowywał odpowiedzi na żądania
 * @param fd_shm_res Uchwyt do operacji zarządzania pamięcią
 * @param memory_ptr_res Wskaźnik do utworzonej pamięci
 */
void memory_create_res(int *fd_shm_res, struct memory_res **memory_ptr_res)
{
    if ((*fd_shm_res = shm_open (MEMORY_RES, O_RDWR | O_CREAT, 0660)) == -1)
        report_and_exit("shm_open res");

    if (ftruncate(*fd_shm_res, sizeof (struct memory_res)) == -1)
        report_and_exit("ftruncate res");

    *memory_ptr_res = mmap(NULL, sizeof(struct memory_res), PROT_READ | PROT_WRITE, MAP_SHARED, *fd_shm_res, 0);
    if (*memory_ptr_res == MAP_FAILED) {
        report_and_exit("mmap res");
    }
}

/**
 * Funkcja tworzy pamięć współdzieloną, która będzie używana do trzymania meta-informacji o operacjach
 * @param fd_shm_res Uchwyt do operacji zarządzania pamięcią
 * @param memory_ptr_res Wskaźnik do utworzonej pamięci
 */
void memory_create_inf(int *fd_shm_inf, struct memory_inf **memory_ptr_inf)
{
    if ((*fd_shm_inf = shm_open (MEMORY_INF, O_RDWR | O_CREAT, 0660)) == -1)
        report_and_exit("shm_open inf");

    if (ftruncate(*fd_shm_inf, sizeof (struct memory_inf)) == -1)
        report_and_exit("ftruncate inf");

    *memory_ptr_inf = mmap(NULL, sizeof(struct memory_inf), PROT_READ | PROT_WRITE, MAP_SHARED, *fd_shm_inf, 0);
    if (*memory_ptr_inf == MAP_FAILED) {
        report_and_exit("mmap inf");
    }
}

/**
 * Funkcja tworzy pamięc współdzieloną, która będzie używana do przechowywania aktualnej wartości wskaźnika
 * @param fd_shm_res Uchwyt do operacji zarządzania pamięcią
 * @param memory_ptr_res Wskaźnik do utworzonej pamięci
 */
void memory_create_cur(int *fd_shm_cur, struct memory_cur **memory_ptr_cur)
{
    if ((*fd_shm_cur = shm_open (MEMORY_CUR, O_RDWR | O_CREAT, 0660)) == -1)
        report_and_exit("shm_open cur");

    if (ftruncate(*fd_shm_cur, sizeof (struct memory_cur)) == -1)
        report_and_exit("ftruncate cur");

    *memory_ptr_cur = mmap(NULL, sizeof(struct memory_cur), PROT_READ | PROT_WRITE, MAP_SHARED, *fd_shm_cur, 0);
    if (*memory_ptr_cur == MAP_FAILED) {
        report_and_exit("mmap cur");
    }
}


#endif //PRACA_INZ_MEMORY_SHARED_H
