/**
 * @file memory_broker.c
 * @brief Zawiera kod brokera pamięci współdzielonej
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <mqueue.h>
#include <errno.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/signalfd.h>

#include "memory_shared.h"
#include "./../includes/signal_local.h"
#include "./../includes/epoll_local.h"

void fetch_cursor_from_signal(int sig_no, struct signalfd_siginfo *sfd_si, unsigned *cur, unsigned *pid);

/**
 * Zmienna przechowująca czytelne dla człowieka nazwy sygnałów
 */
extern const char * const sys_siglist[];

/**
 * Zmienna sterująca, określająca czy program powinien zakończyć działanie.
 * Jest nadpisywana pod odczytaniu odpowiedniego sygnału.
 */
char quit;

/**
 * Funkcja główna programu
 * @return int
 */
int main (void)
{
    struct epoll_event events[32];
    struct memory_req *memory_ptr_req;
    struct memory_res *memory_ptr_res;
    struct memory_inf *memory_ptr_inf;
    struct memory_cur *memory_ptr_cur;
    struct mq_attr attr;

    mqd_t qfd, qfd_client;

    int fd_shm_req, fd_shm_res, fd_shm_inf, fd_shm_cur;
    char qfd_message[QUEUE_MESSAGE_SIZE];
    pid_t pid = getpid();

    memory_create_req(&fd_shm_req,&memory_ptr_req);
    memory_create_res(&fd_shm_res,&memory_ptr_res);
    memory_create_inf(&fd_shm_inf,&memory_ptr_inf);
    memory_create_cur(&fd_shm_cur,&memory_ptr_cur);

    memory_ptr_cur->cursor = 0;

    attr.mq_flags = 0;
    attr.mq_maxmsg = QUEUE_MAX_MESSAGES;
    attr.mq_msgsize = QUEUE_MESSAGE_SIZE;
    attr.mq_curmsgs = 0;
    if ((qfd = mq_open(CURSOR_SERVER, O_RDONLY | O_CREAT, QUEUE_PERMISSIONS, &attr)) == -1) {
        perror ("mq_open");
        exit (errno);
    }
    fcntl(qfd, F_SETFL, fcntl(qfd, F_GETFL, 0) | O_NONBLOCK);

    int signal_fd;
    signal_attach(&signal_fd);
    struct signalfd_siginfo sfd_si;

    int epfd = epoll_create(1);
    epoll_add(epfd, qfd, EPOLLIN);
    epoll_add(epfd, signal_fd, EPOLLIN);

    unsigned u_cur, u_pid;
    int nfds;
    for (;;) {
        if (quit == 1)
            break;
        nfds = epoll_wait(epfd, events, 32, -1);
        if (nfds == -1) {
            perror ("epoll_wait");
            exit (errno);
        }

        char cursor_value[8];
        for (int i = 0; i < nfds; i++) {
            if (events[i].data.fd == qfd) {
                memset(qfd_message, 0, QUEUE_MESSAGE_SIZE);
                if (mq_receive(qfd, qfd_message, QUEUE_MESSAGE_SIZE, NULL) == -1) {
                    perror ("mq_receive");
                    exit (errno);
                }
                if ((qfd_client = mq_open(qfd_message, O_WRONLY)) == -1) {
                    perror ("mq_open qfd_client");
                    exit (errno);
                }

                memory_ptr_cur->cursor++;
                if (memory_ptr_cur->cursor >= MAX_CURSOR - 1)
                    memory_ptr_cur->cursor = 0;

                memset(cursor_value, 0, sizeof(cursor_value));
                sprintf(cursor_value, "%d %d", pid, memory_ptr_cur->cursor);
                if (mq_send(qfd_client, cursor_value, strlen(cursor_value) + 1, 0) == -1) {
                    perror("mq_send");
                }
            } else if (events[i].data.fd == signal_fd) {
                signal_read(signal_fd, &sfd_si);
                int sig_no = sfd_si.ssi_signo;
                if (sig_no == SIGUSR1 || sig_no == SIGUSR2) { // queue_client zakończył
                    fetch_cursor_from_signal(sig_no, &sfd_si, &u_cur, &u_pid);

                    printf("[+] user data: %s\n", memory_ptr_req->req[u_cur]);
                } else if (sig_no == SIGINT || sig_no == SIGHUP || sig_no == SIGTERM || sig_no == SIGQUIT) {
                    printf("[+] received term signal: %s\n", sys_siglist[sig_no]);
                    quit = 1;
                }
            }
        }
    }
}

/**
 * Funkcja odczytująca wartość kursora z danych przychodzących do procesu przez sygnały wysłane z sigqueue()
 * @param sig_no Numer sygnału: SIGINT, SIGUSR1 ...
 * @param sfd_si Struktura siginfo zawierająca dodatkowe dane użytkownika
 * @param cur Wskaźnik do zmiennej przechowującej wartość kursora
 * @param pid Wskaźnik do zmiennej przechowującej PID procesu
 */
void fetch_cursor_from_signal(int sig_no, struct signalfd_siginfo *sfd_si, unsigned *cur, unsigned *pid)
{
    printf("[+] incoming user signal: %d\n", sig_no);
    if (sfd_si->ssi_code == SI_QUEUE) {
        int sig_int = sfd_si->ssi_int;
        unsigned sig_pid = sfd_si->ssi_pid;
        unsigned sig_uid = sfd_si->ssi_uid;

        printf("[+] signal from pid: %d, uid: %d, value: %d\n", sig_pid, sig_uid, sig_int);
        if (sig_no == SIGUSR1) {
            sig_int -= 30000;
        } else if (sig_no == SIGUSR2) {
            sig_int -= 40000;
        }
        *cur = (unsigned) sig_int;
        *pid = sig_pid;
        printf("[+] fetched cursor value: %d\n", sig_int);
    }
}










