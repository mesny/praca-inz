/**
 * @file sigqueue.c
 * @brief Kod źródłowy programu który wysyła określony sygnał do wybranego procesu za pomocą aktualnego API sygnałów
 */

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <stdbool.h>

/**
 * Funkcja wysyła komunikat na STDERR i opcjonalnie kończy działanie programu
 * @param message Tekst komunikatu
 * @param terminate
 */
void report(const char *message, bool terminate)
{
    perror(message);
    if (terminate)
        exit(EXIT_FAILURE);
}

/**
 * Program wysyłający sygnał za pomocą funkcji sigqueue().
 * Przyjmuje numer sygnału i docelowy pid z linii poleceń.
 * @param argc Ilość argumentów
 * @param argv Wartości przekazanych argumentów
 * @return
 */
int main (int argc, char *argv[])
{
    pid_t pid = atoi(argv[1]);
    unsigned int sig = atoi(argv[2]);
    unsigned int val = atoi(argv[3]);

    printf("[+] sending signal %d to pid %d, with data %d\n", sig, pid, val);

    union sigval value;
    value.sival_int = val;
    if (sigqueue(pid, sig, value) == 0) {
        printf("[+] signal sent.\n");
    } else {
        perror("sigqueue");
    }

    return 0;
}
