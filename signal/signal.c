/**
 * @file signal.c
 * @brief Plik zawiera krótką prezentację działania przestarzałego API do obsługi sygnałów.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

/**
 * Tablica zawierająca czytelne dla człowieka nazwy sygnałów.
 */
extern const char * const sys_siglist[];

/**
 * Prosty handler sygnału INT
 * @param signo
 */
static void signal_handler_sigint (int signo)
{
    printf ("signal_handler_sigint: caught signal %s\n", sys_siglist[signo]);
}

/**
 * Prosty handler sygnału TERM
 * @param signo
 */
static void signal_handler_sigterm (int signo)
{
    printf ("signal_handler_sigterm: caught signal %s\n", sys_siglist[signo]);
}

/**
 * Prosty handler sygnału QUIT
 * @param signo
 */
static void signal_handler_sigquit (int signo)
{
    printf ("[+] signal_handler_sigquit: caught signal %s\n", sys_siglist[signo]);
}

/**
 * Prosty handler sygnału USR1 i USR2
 * @param signo
 */
static void signal_handler_sigusr(int signo)
{
    printf("[+] signal_handler_sigusr: caught user-defined signal %s\n", sys_siglist[signo]);
}

/**
 * Funkcja główna programu
 * @return
 */
int main (void)
{
    unsigned int handled_signals_n = 0;
    pid_t pid = getpid();

    printf("[+] pid: %d\n", pid);
    fflush(stdout);

    if (signal(SIGINT, signal_handler_sigint) == SIG_ERR) {
        fprintf (stderr, "Cannot handle signal: SIGINT!\n");
        exit (EXIT_FAILURE);
    }
    if (signal(SIGTERM, signal_handler_sigterm) == SIG_ERR) {
        fprintf (stderr, "Cannot handle signal: SIGTERM!\n");
        exit (EXIT_FAILURE);
    }
    if (signal(SIGQUIT, signal_handler_sigquit) == SIG_ERR) {
        fprintf (stderr, "Cannot handle signal: SIGQUIT!\n");
        exit (EXIT_FAILURE);
    }
    if (signal(SIGUSR1, signal_handler_sigusr) == SIG_ERR) {
        fprintf (stderr, "Cannot handle signal: SIGUSR1!\n");
        exit (EXIT_FAILURE);
    }
    if (signal(SIGUSR2, signal_handler_sigusr) == SIG_ERR) {
        fprintf (stderr, "Cannot handle signal: SIGUSR2!\n");
        exit (EXIT_FAILURE);
    }
    if (signal(SIGPROF, SIG_DFL) == SIG_ERR) {
        fprintf (stderr, "Cannot reset handler for signal: SIGPROF!\n");
        exit (EXIT_FAILURE);
    }
    if (signal(SIGHUP, SIG_IGN) == SIG_ERR) {
        fprintf (stderr, "Cannot ignore signal: SIGHUP!\n");
        exit (EXIT_FAILURE);
    }

    for (;;)
        if (pause() == -1 && errno == EINTR) {
            handled_signals_n++;
            printf("[+] %d signals so far\n", handled_signals_n);
        }

    return 0;
}
