/**
 * @file sigaction.c
 * @brief Kod źródłowy programu prezentującego działanie aktualnego API obsługi sygnałów
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <stdbool.h>
#include <string.h>
#include <sys/time.h>

/**
 * Tablica przechowująca czytelna dla człowieka nazwy sygnałów
 */
extern const char * const sys_siglist[];

/**
 *
 * @param message
 * @param terminate
 */
void report(const char *message, bool terminate)
{
    perror(message);
    if (terminate)
        exit(EXIT_FAILURE);
}

/**
 * Funkcja obsługująca sygnały z obsługą struktury siginfo_t
 * @param signo Numer sygnału
 * @param siginfo Dodatkowe dane o sygnale
 * @param context
 */
static void signal_handler (int signo, siginfo_t *siginfo, void *context)
{
    pid_t sender_pid = siginfo->si_pid;
    uid_t sender_uid = siginfo->si_uid;
    unsigned int value;

    if (siginfo->si_code == SI_KERNEL) {
        printf("[+] signal: %s, reason: SI_KERNEL\n", sys_siglist[signo]);

    } else if (siginfo->si_code == SI_TIMER) {
        printf("[+] signal: %s, reason: SI_TIMER\n", sys_siglist[signo]);

    } else if (siginfo->si_code == SI_QUEUE) {
        value = (unsigned int) siginfo->si_value.sival_int;
        printf("[+] signal: %s, from user: %d, pid: %d, value: %d\n", sys_siglist[signo], sender_uid, sender_pid, value);

    } else if (siginfo->si_code == SI_USER) {
        printf("[+] signal: %s, from user: %d, pid: %d, reason: SI_USER\n", sys_siglist[signo], sender_uid, sender_pid);

    }
}

/**
 * Instaluje zegar czasu rzeczywistego z interwałem 10-sekundowym
 */
static void install_timer()
{
    struct itimerval delay;
    int ret;

    delay.it_value.tv_sec = 5;
    delay.it_value.tv_usec = 0;
    delay.it_interval.tv_sec = 10;
    delay.it_interval.tv_usec = 0;

    ret = setitimer(ITIMER_REAL, &delay, NULL);
    if (ret) {
        report("setitimer", true);
    }
}

/**
 * Instaluje funkcje obsługi sygnałów
 * @param signo Numer sygnału
 * @param handler_function Wskaźnik do funkcji która powinna obsługiwać wybrany sygnał
 */
static void install_handler(unsigned int signo, void (*handler_function)(int signo, siginfo_t *siginfo, void *context))
{
    struct sigaction handler;
    handler.sa_sigaction = handler_function;
    handler.sa_flags = SA_SIGINFO;
    if (sigaction(signo, &handler, NULL) < 0) {
        report ("install_handler: sigaction", true);
    }
}

/**
 * Funkcja główna programu
 * Program instaluje funkcje obsługujące wybrane sygnały,
 * instaluje zegar czasu rzeczywistego z interwałem 10-sekundowym i w nieskończonej pętli wykonuje funkcję pause()
 * w oczekiwaniu na sygnały.
 * @return
 */
int main (void)
{
    unsigned int handled_signals_n = 0;
    pid_t pid = getpid();

    printf("[+] pid: %d\n", pid);
    fflush(stdout);

    install_handler(SIGINT, &signal_handler);
    install_handler(SIGHUP, &signal_handler);
    install_handler(SIGQUIT, &signal_handler);
    install_handler(SIGTERM, &signal_handler);
    install_handler(SIGALRM, &signal_handler);
    install_handler(SIGUSR1, &signal_handler);
    install_handler(SIGUSR2, &signal_handler);

    install_timer();

    for (;;)
        if (pause() == -1 && errno == EINTR) {
            handled_signals_n++;
            printf("[+] %d signals so far\n", handled_signals_n);
        }

    return 0;
}
