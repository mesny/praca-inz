/**
 * @file epoll_server.c
 * @brief Kod źródłowy serwera słuchającego na gnieździe domeny UNIX
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>

#include "./../includes/epoll_local.h"
#include "./../includes/signal_local.h"

#define MAX_BUF 1024
#define MAX_EPOLL_EVENTS 64

/**
 * Funkcja główna programu; serwer pracujący na gniazdach UNIX.
 * @return
 */
int main(void)
{
    struct sockaddr_un server_addr;
    struct sockaddr_un client_addr;
    struct epoll_event events[32];

    char message_buffer[256];
    const char *socket_file = "/tmp/ahns.sock";
    unlink(socket_file);

    int signal_fd, sig_no;
    signal_attach(&signal_fd);
    struct signalfd_siginfo sfd_si;

    memset((void *) &server_addr, 0, sizeof(server_addr));
    server_addr.sun_family = AF_UNIX;
    strcpy((void *) &(server_addr.sun_path), socket_file);

    int server_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    bind(server_fd, (const struct sockaddr *) &server_addr, sizeof(server_addr));
    listen(server_fd, 10);

    int epfd = epoll_create(1);
    epoll_add(epfd, server_fd, EPOLLIN);
    epoll_add(epfd, signal_fd, EPOLLIN);
    ualarm(1, 900000);

    int new_connection, nfds, n;
    unsigned loop = 1;
    for (;;) {
        if (loop == 0)
            break;

        nfds = epoll_wait(epfd, events, MAX_EPOLL_EVENTS, -1);
        if (nfds < 0) {
            perror ("epoll_wait");
            exit (EXIT_FAILURE);
        }
        for (int i = 0; i < nfds; i++) {
            if (events[i].data.fd == server_fd) { // accepting new connection
                unsigned int sockaddr_len = sizeof(client_addr);
                new_connection = accept(server_fd, (struct sockaddr *) &client_addr, &sockaddr_len);
                if (new_connection == -1) {
                    perror("accept");
                    exit(EXIT_FAILURE);
                }
                fcntl(new_connection, F_SETFL, fcntl(new_connection, F_GETFL, 0) | O_NONBLOCK);
                printf("[+] client connected\n");
                epoll_add(epfd, new_connection, EPOLLIN | EPOLLHUP);

            } else if (events[i].data.fd == signal_fd) { // receiving signal
                memset((void *) &sfd_si, 0, sizeof (sfd_si));
                signal_read(signal_fd, &sfd_si);
                sig_no = sfd_si.ssi_signo;

                if (sig_no == SIGINT || sig_no == SIGQUIT || sig_no == SIGTERM) {
                    printf("[+] quit signal received\n");
                    loop = 0;
                }
            } else {    // peers related events
                if (events[i].events & EPOLLIN) { // peer sends data
                    for (;;) {
                        memset((void *) message_buffer, 0, sizeof(message_buffer));
                        n = read(events[i].data.fd, message_buffer, sizeof(message_buffer));
                        if (n <= 0 /* || errno == EAGAIN */ ) {
                            break;
                        } else {
                            printf("[+] received: %s\n", message_buffer);
                        }
                    }
                }
                if (events[i].events & EPOLLHUP) { // client disconnected
                    printf("[+] client disconnected\n");

                    epoll_del(epfd, events[i].data.fd);
                    close(events[i].data.fd);
                    continue;
                }
            }
        }
    }

    return 0;
}