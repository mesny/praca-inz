/**
 * @file epoll_client.c
 * @brief Kod źródłowy programu pracującego na gnieździe UNIX w roli klienta
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <resolv.h>
#include <sys/socket.h>
#include <sys/epoll.h>
//#include <arpa/inet.h>
#include <sys/un.h>
#include <unistd.h>

#include "./../includes/epoll_local.h"
#include "./../includes/signal_local.h"
#include "./../includes/time_local.h"

/**
 * Maksymalna długość pojedynczego komunikatu
 */
#define MAX_BUF 1024

/**
 * Ile zdarzeń IO program może obsłużyć w pojedynczej iteracji pętli głównej
 */
#define MAX_EPOLL_EVENTS 64

/**
 * Tablica przechowująca nazwy sygnałów.
 */
extern const char * const sys_siglist[];

/**
 * Funkcja główna programu.
 * @param argc Ilość przekazanych argumentów
 * @param argv Wartości argumentów
 * @return
 */
int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("[e] invalid arguments");
        exit(EXIT_FAILURE);
    }
    char program_name[20];
    strncpy(program_name, argv[1], 19);

    int socket_fd, epoll_fd;
    struct sockaddr_un sock_dest;
    struct epoll_event events[MAX_EPOLL_EVENTS];

    int i, num_ready;
    char buffer[MAX_BUF];
    const char *socket_file = "/tmp/ahns.sock";

    epoll_fd = epoll_create(1);
    if ((socket_fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0) {
        perror("socket");
        exit(errno);
    }

    memset((void *) &sock_dest, 0, sizeof(sock_dest));
    sock_dest.sun_family = AF_UNIX;
    strcpy(sock_dest.sun_path, socket_file);

    epoll_add(epoll_fd, socket_fd, EPOLLIN | EPOLLOUT | EPOLLHUP);
    if (connect(socket_fd, (struct sockaddr*) &sock_dest, sizeof(sock_dest)) != 0) {
        if (errno != EINPROGRESS) {
            perror("connect");
            exit(errno);
        }
    }

    epoll_wait(epoll_fd, events, 1, 1000);
    if (events[0].events & (EPOLLIN | EPOLLOUT)) {
        printf("[+] connected to server\n");
    } else {
        printf("[e] failed to connect.\n");
        exit(errno);
    }

    int signal_fd, sig_no;
    signal_attach(&signal_fd);
    struct signalfd_siginfo sfd_si;

    epoll_add(epoll_fd, signal_fd, EPOLLIN);
    ualarm(1, 900000);

    unsigned loop = 1, send_time = 0, sending_time = 0;
    char dt[80];
    ssize_t n;
    for (;;) {
        if (loop == 0)
            break;

        num_ready = epoll_wait(epoll_fd, events, MAX_EPOLL_EVENTS, -1);
        for (i = 0; i < num_ready; i++) {
            if (events[i].data.fd == signal_fd) {
                memset((void *) &sfd_si, 0, sizeof (sfd_si));

                signal_read(signal_fd, &sfd_si);
                sig_no = sfd_si.ssi_signo;
                if (sig_no == SIGALRM && sending_time == 1) {
                    send_time = 1;
                } else if (sig_no == SIGINT) {
                    if (sending_time == 0) {
                        printf("[+] sending datetime enabled\n");
                        sending_time = 1;
                    } else {
                        printf("[+] sending datetime disabled\n");
                        sending_time = 0;
                    }
                } else if (sig_no == SIGQUIT) {
                    loop = 0;
                }
            }
            if (events[i].data.fd == socket_fd) {
                memset((void *) buffer, 0, MAX_BUF);
                if (events[i].events & EPOLLIN) {
                    recv(socket_fd, buffer, sizeof(buffer), 0);
                    printf("[+] received %d bytes: \"%s\"\n", (int) strlen(buffer), buffer);
                }
                if (events[i].events & EPOLLOUT) {
                    if (send_time) {
                        memset(dt, 0, sizeof dt);
                        dt_string(dt, sizeof (dt));
                        sprintf(buffer, "%s %s", program_name, dt);

                        n = send(socket_fd, buffer, strlen(buffer), 0);
//                        printf("[+] sent %d bytes\n", (int) n);
                        send_time = 0;
                    }
                }
                if (events[i].events & EPOLLHUP) {
                    printf("[+] EPOLLHUP\n");
                    loop = 0;
                }
            }
        }
    }

    printf("[+] closing\n");

    close(socket_fd);
    close(signal_fd);
    return 0;
}