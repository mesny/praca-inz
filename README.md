# Projekt inżynierski

_Michał Szczęsny_

_Numer indeksu 7072_

_Tytuł projektu:_
## Przegląd mechanizmów komunikacji międzyprocesowej w środowisku Linux, z przykładowymi programami zaimplementowanymi w języku C



### Struktura katalogów ###

* `includes` - pliki współdzielone
* `memory` - mechanizm współdzielenia pamięci między procesami, z użyciem funkcji `mmap()` 
* `queue` - kolejki komunikatów POSIX, wspierane przez jądro
* `signal` - API do obsługi sygnałów, tradycyjne `signal` i aktualne `sigqueue` i `sigaction`
* `socket` - serwer gniazd UNIX, z wykorzystaniem mechanizmu powiadomień o zdarzeniach `epoll` 
* `socketpair` - funkcjonalność tworzenia par połączonych gniazd za pomocą funkcji `socketpair()`


