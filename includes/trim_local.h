/**
 * @file trim_local.h
 * @brief Plik zawiera funkcje do trymowania ciągów znaków
 */

#ifndef PRACA_INZ_TRIM_LOCAL_H
#define PRACA_INZ_TRIM_LOCAL_H

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/**
 * Funkcja obcina puste, drukowalne znaki z obu stron zmiennej typu string.
 * Funkcje modyfikuje argument "in-place", poprzez użycie wskaźnika.
 *
 * @param s Ciąg znaków do przetworzenia
 * @param ending Ciąg znaków którym należy zakończyć zmienną s
 * @return
 */
char *trim_inplace(char *s, const char *ending) {
    char *ptr;
    if (!s)
        return NULL;   // handle NULL string
    if (!*s)
        return s;      // handle empty string
    for (ptr = s + strlen(s) - 1; (ptr >= s) && isspace(*ptr); --ptr);
    ptr[1] = *ending;
    return s;
}

/**
 * Funkcje kopiuje ciąg znaków ze wskaźnika z pierwszego argumentu,
 * do wskaźnika przekazanego w drugim argumencie.
 *
 * @param dest Wskaźnik do zmiennej która będzie zapisana
 * @param src Wskaźnik do zmiennej z której należy skopiować dane
 * @param ending Ciąg znaków którym należy zakończyć docelową zmienną
 */
void trim_copy(char *dest, char *src, const char *ending) {
    if (!src || !dest)
        return;
    int len = strlen (src);
    if (!len) {
        *dest = '\0';
        return;
    }

    char *ptr = src + len - 1;
    while (ptr > src) {
        if (!isspace (*ptr))
            break;
        ptr--;
    }

    ptr++;
    char *q;
    for (q = src; (q < ptr && isspace (*q)); q++)
        ;
    while (q < ptr)
        *dest++ = *q++;
    *dest = *ending;
}

#endif //PRACA_INZ_TRIM_LOCAL_H
