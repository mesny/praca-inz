/**
 * @file epoll_local.h
 * @brief Zawiera funkcje pomocnicze do epoll
 */

#ifndef PRACA_INZ_EPOLL_LOCAL_H
#define PRACA_INZ_EPOLL_LOCAL_H

#include <sys/epoll.h>

/**
 * Dodanie deskryptora pliku do instancji epoll w celu monitorowania zdarzeń IO
 * @param epfd Uchwyt instancji epoll
 * @param fd Deskryptor pliku
 * @param events O jakich zdarzeniach IO chcemy byc powiadomieni
 */
void epoll_add(int epfd, int fd, int events)
{
    struct epoll_event ev;
    ev.data.fd = fd;
    ev.events = events;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
        perror("epoll_ctl EPOLL_CTL_ADD");
        exit(EXIT_FAILURE);
    }
}

/**
 * Usunięcie wskazanego deskryptora pliku z monitorowania zdarzeń IO.
 * @param epfd Uchwyt instancji epoll
 * @param fd Deskryptor pliku dla którego chcemy zakończyć monitorowanie
 */
void epoll_del(int epfd, int fd)
{
    if (epoll_ctl(epfd, EPOLL_CTL_DEL, fd, NULL) == -1) {
        perror("epoll_ctl EPOLL_CTL_DEL");
        exit(EXIT_FAILURE);
    }
}

/**
 * Modyfikacja sposobu monitorowania zdarzeń IO dla wybranego deskryptora pliku
 * @param epfd Deskryptor pliku mechanizmu epoll
 * @param fd Monitorowany deskryptor pliku
 * @param events Mapa bitowa monitorowanych zdarzeń
 */
void epoll_mod(int epfd, int fd, int events)
{
    struct epoll_event ev;
    ev.data.fd = fd;
    ev.events = events;
    if (epoll_ctl(epfd, EPOLL_CTL_MOD, fd, &ev) == -1) {
        perror("epoll_ctl EPOLL_CTL_MOD");
        exit(EXIT_FAILURE);
    }
}

#endif //PRACA_INZ_EPOLL_LOCAL_H
