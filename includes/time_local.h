/**
 * @file time_local.h
 * @brief Plik zawiera funkcje zwracającą czytelny dla użytkownika zapis aktualnego czasu, razem z milisekundami
 */

#ifndef PRACA_INZ_TIME_LOCAL_H
#define PRACA_INZ_TIME_LOCAL_H

#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

/**
 * Funkcja nadpisująca pierwszy argument ciągiem znkaów reprezentującym aktualny czas, łącznie z milisekundami
 *
 * @param datetime_string Zmienna która będzie nadpisana aktualnym czasem
 * @param length Maksymalna długość zmiennej
 */
void dt_string(char datetime_string[], ssize_t length)
{
    char dt[20];
    int milliseconds;
    struct tm* tm_info;
    struct timeval tv;

    gettimeofday(&tv, NULL);
    milliseconds = lrint(tv.tv_usec / 1000.0);
    if (milliseconds>=1000) {
        milliseconds -=1000;
        tv.tv_sec++;
    }

    tm_info = localtime(&tv.tv_sec);
    strftime(dt, 20, "%F %T", tm_info);
    snprintf(datetime_string,length, "%s.%03d", dt, milliseconds);
    // 2022-03-24 14:26:12.123
}

#endif //PRACA_INZ_TIME_LOCAL_H
