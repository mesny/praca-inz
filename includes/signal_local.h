/**
 * @file signal_local.h
 * @brief Zawiera funkcje biblioteczne do obsługi sygnałów
 */

#ifndef PRACA_INZ_SIGNAL_LOCAL_H
#define PRACA_INZ_SIGNAL_LOCAL_H

#include <signal.h>
#include <sys/signalfd.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

void signal_mask_create(sigset_t *mask);

extern const char * const sys_siglist[];

/**
 * Funkcja odczytuje sygnał ze specjalnego deskryptora pliku.
 * @param sfd Deskryptor pliku używany do odczytywania sygnałów
 * @param sfd_si Specjalna struktura używana do obsługi sygnałów przychodzących do deskryptora sygnałów
 */
void signal_read(int sfd, struct signalfd_siginfo *sfd_si)
{
    int s = read(sfd, sfd_si, sizeof(*sfd_si));
    if (s != sizeof(*sfd_si)) {
        perror("signal_read read");
        exit(errno);
    }

}

/**
 * Funkcje przekierowuje sygnały do wskazanego deskryptora pliku.
 * Funkcja powoduje zmianę mechanizmu obsługi sygnałów, dla tych sygnałów które są wskazane w masce.
 * Po poprawnym wykonaniu funkcji, na deskryptorze pliku można wykonywać standardowe operacje odczytu, bo będzie powodowało odczytywanie nadchodzących sygnałów.
 * @param signal_fd Deskryptor pliku, który będzie używany do odczytywania nadchodzących sygnałów.
 */
void signal_attach(int *signal_fd)
{
    sigset_t mask;
    signal_mask_create(&mask);
    if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1) {
        perror("signal_attach sigprocmask");
        exit(errno);
    }

    int fd = signalfd(-1, &mask, 0);
    if (fd == -1) {
        perror("signal_attach signalfd");
        exit(errno);
    }

    fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK);
    *signal_fd = fd;
}

/**
 * Funkcja przesyła sygnał do wyznaczonego procesu.
 * Dzięki użyciu funkcji sigqueue() zamiast kill() wysłanie sygnału przeprowadzone jest w sposób bezpieczny.
 * @param pid PID procesu do którego chcemy wysłać sygnał
 * @param sig_no Numer sygnału do wysłania
 * @param value Pole na dodatkową wartość użytkownika
 * @return int
 */
int signal_notify(unsigned pid, unsigned sig_no, int value)
{
    if (sig_no == SIGUSR1) { // issue request
        value += 30000;
    } else if (sig_no == SIGUSR2) { // see response
        value += 40000;
    }

    int ret;
    union sigval usv;
    usv.sival_int = value;
    ret = sigqueue(pid, sig_no, usv);
    if (ret != 0) {
        perror("signal_notify sigqueue");
        exit(errno);
    }

    printf("[+] signal sent\n");
    return ret;
}

/**
 * Funkcja tworząca maskę obsługi sygnałów.
 *
 * @param mask Wskaźnik do zmiennej reprezentującej maskę
 */
void signal_mask_create(sigset_t *mask)
{
    sigemptyset(mask);
    sigaddset(mask, SIGINT);
    sigaddset(mask, SIGHUP);
    sigaddset(mask, SIGTERM);
    sigaddset(mask, SIGALRM);
    sigaddset(mask, SIGQUIT);
    sigaddset(mask, SIGUSR1);
    sigaddset(mask, SIGUSR2);
}

#endif //PRACA_INZ_SIGNAL_LOCAL_H
