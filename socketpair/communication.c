/**
 * @file communication.c
 * @brief Program prezentuje działanie funkcji socketpair() w połączeniu w funkcją fork().
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>

#include "./../includes/trim_local.h"

#define MAX_LENGTH 512

/**
 *
 * @param message
 * @param terminate
 */
void report(const char *message, bool terminate)
{
    perror(message);
    if (terminate)
        exit(EXIT_FAILURE);
}

/**
 * Główna funkcja procesu macierzystego.
 * Funkcje odbiera znaki wpisane przez użytkownika na STDIN procesu do zmiennej char in[],
 * obcina puste znaki z obu stron zmiennej, i przekazuje odczytane znaki do gniazda UNIX utworzonego
 * za pomocą funkcji socketpair().
 *
 * @param stream Strumień zawierający jedno z dwóch połączonych gniazd zwróconych przez socketpair()
 */
void parent(FILE * restrict stream)
{
    pid_t pid = getpid();
    fprintf(stdout, "%d: parent\n", (int) pid);
    fflush(stdout);

    char in[MAX_LENGTH];
    char trimmed[MAX_LENGTH];

    for (;;) {
        memset(in, 0, MAX_LENGTH);
        memset(trimmed, 0, MAX_LENGTH);
        if (fgets(in, MAX_LENGTH - 1, stdin) == NULL) {
            report("parent fgets", true);
        }

        trim_copy(trimmed, in, "\n\0");
        if (fputs(trimmed, stream) == EOF) {
            report("parent fputs", true);
        }
        trim_inplace(trimmed, "\0");
        fprintf(stdout, "[+] %d: parent stdin: %s\n", pid, trimmed);
        fprintf(stdout, "[+] %d: pushing to child: %s\n", pid, trimmed);

        fprintf(stdout, "[+] %d: parent successfully pushed to child\n", pid);
        fflush(stdout);
    }

}

/**
 * Główna funkcja uruchomiona w procesie potomnym.
 * Funkcje odbiera znaki wpisane przez użytkownika do procesu macierzystego, do zmiennej in[],
 * obcina puste znaki z obu stron zmiennej, i drukuje wpisane dane na STDOUT procesu.
 *
 * @param stream Strumień zawierający jedno z dwóch połączonych gniazd zwróconych przez socketpair()
 */
void child(FILE * restrict stream)
{
    pid_t pid = getpid();
    fprintf(stdout, "[+] %d: child\n", pid);
    fflush(stdout);

    char in[MAX_LENGTH];
    char trimmed[MAX_LENGTH];

    for (;;) {
        memset(in, 0, MAX_LENGTH);
        memset(trimmed, 0, MAX_LENGTH);
        if (fgets(in, MAX_LENGTH, stream) == NULL) {
            report("child fgets", true);
        }
        trim_copy(trimmed, in, "\0");
        fprintf(stdout, "[+] %d: child received: %s\n", pid, trimmed);
        fflush(stdout);
    }
}

/**
 * Funkcja główna programu.
 * Program wykonuje następujące czynności:
 * 1. Ustawia parametry buforowania STDOUT i STDERR na buforowanie do znaku końca linii, z limitem 1kb.
 * 2. Tworzy dwa połączone, bliźniacze gniazda UNIX, do komunikacji międzyprocesowej.
 * 3. Rozwidla proces za pomocą funkcji fork()
 * 4. W procesie macierzystym uruchamia funkcję parent()
 * 5. W procesie potomnym uruchamia funkcję child()
 *
 * @return
 */
int main (void)
{
    int fd[2];
    FILE * restrict stream;
    pid_t pid;

    setvbuf(stdout, NULL, _IOLBF, 1024);
    setvbuf(stderr, NULL, _IOLBF, 1024);

    if (socketpair(AF_UNIX, SOCK_STREAM, 0, fd) != 0) {
        report("socketpair", true);
    }

    pid = fork();
    if (pid == -1) {
        report("fork", true);
    } else if (pid > 0) {                      // parent
        close(fd[1]);
        stream = (FILE * restrict) fdopen(fd[0], "r+b");
        setvbuf(stream, NULL, _IOLBF, MAX_LENGTH);
        if (stream == NULL) {
            report("fdopen", true);
        }

        parent(stream);

    } else {                                     // child
        close(fd[0]);
        stream = (FILE * restrict) fdopen(fd[1], "r+b");
        setvbuf(stream, NULL, _IOLBF, MAX_LENGTH);
        if (stream == NULL) {
            report("fdopen", true);
        }

        child(stream);

    }
}