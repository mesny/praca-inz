/**
 * @file translation.c
 * @brief Program prezentuje działanie funkcji socketpair() w połączeniu w funkcją fork(), z użyciem struktury hashmap
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "./../includes/trim_local.h"

#define MAX_LENGTH 512

struct nlist {
    struct nlist *next;
    char *name;
    char *defn;
};

#define HASHSIZE 101
static struct nlist *hashtab[HASHSIZE];

/**
 * Funkcja tworzy numeryczny hash w podanej wartości.
 * @param s Ciąg znaków dla którego chcemy znaleźć hash.
 * @return Wyliczona wartość hash
 */
unsigned hash(char *s)
{
    unsigned hashval;
    for (hashval = 0; *s != '\0'; s++)
        hashval = *s + 31 * hashval;

    return hashval % HASHSIZE;
}

/**
 * Funkcja wyszukuje podanego ciągu znaków w słowniku zbudowanym za pomocą struktury danych hashmap
 * @param s Wyszukiwane słowo
 * @return Struktura słowa nlist
 */
struct nlist *lookup(char *s)
{
    struct nlist *np;
    for (np = hashtab[hash(s)]; np != NULL; np = np->next)
        if (strcmp(s, np->name) == 0)
            return np;
            /* found */
    return NULL;
    /* not found */
}

/**
 * Funkcja dodaje do struktury typu hashmap słownika nowe słowo wraz z tłumaczeniem.
 * @param name Słowo
 * @param defn Tłumaczenie
 * @return
 */
struct nlist *install(char *name, char *defn)
{
    struct nlist *np;
    unsigned hashval;

    if ((np = lookup(name)) == NULL) {
        np = (struct nlist *) malloc(sizeof(*np));
        if (np == NULL || (np->name = strdup(name)) == NULL)
            return NULL;

        hashval = hash(name);
        np->next = hashtab[hashval];
        hashtab[hashval] = np;
    } else {
        free((void *) np->defn);
    }

    if ((np->defn = strdup(defn)) == NULL)
        return NULL;

    return np;
}

/**
 * Funkcja wysyła komunikat o błędzie na STDERR i opcjonalnie kończy działanie programu.
 * @param message Wiadomość która powinna być wysłana na standardowe wyjście błędu
 * @param terminate Flaga, czy program powinien zakończyć działanie
 */
void report(const char *message, bool terminate)
{
    perror(message);
    if (terminate)
        exit(EXIT_FAILURE);
}

/**
 * Funkcja uruchamiana w procesie macierzystym.
 * Odczytuje dane wpisane przez użytkownika na STDIN i przekazuje je do procesu potomnego przez strumień.
 *
 * @param stream Strumień zawierający jedno z dwóch połączonych gniazd zwróconych przez socketpair()
 */
void parent(FILE * restrict stream)
{
    pid_t pid = getpid();
    fprintf(stdout, "[+] parent started with pid: %d\n", pid);
    fflush(stdout);

    char in[MAX_LENGTH];

    for (;;) {
        memset(in, 0, MAX_LENGTH);
        if (fgets(in, MAX_LENGTH, stdin) == NULL) {
            report("parent fgets", true);
        }

        trim_inplace(in, "\0");
        fprintf(stdout, "[+] stdin: %s\n", in);
        fprintf(stdout, "[+] pushing to child: %s\n", in);

        trim_inplace(in, "\n\0");
        if (fputs(in, stream) == EOF) {
            report("parent fputs", true);
        } else {
            fprintf(stdout, "[+] successfully pushed to child\n");
        }
    }
}

/**
 * Funkcja wykonuje następujące działania:
 * 1. Drukuje na STDOUT własny PID
 * 2. Dodaje słowa do słownika tłumaczeń
 * 3. Odczytuje linie z gniazda UNIX, jedno słowo == jedna linia
 * 4. Sprawdza, czy podane słowo znajduje się w słowniku
 * 5a. Jeżeli słowo znajduje się w słowniku, drukuje na STDOUT tłumaczenie dla podanego słowa
 * 5b. Jeżeli słowo nie znajduje się w słowniku, drukuje informację o braku tłumaczenia dla słowa.
 *
 * @param stream Strumień zawierający jedno z dwóch połączonych gniazd zwróconych przez socketpair()
 */
void child(FILE * restrict stream)
{
    struct nlist *word;

    pid_t pid = getpid();
    fprintf(stdout, "[+] child started with pid: %d\n", pid);
    fflush(stdout);

    install("one", "jeden");
    install("two", "dwa");
    install("three", "trzy");
    install("four", "cztery");
    install("five", "piec");

    char in[MAX_LENGTH];

    for (;;) {
        memset(in, 0, MAX_LENGTH);
        if (fgets(in, MAX_LENGTH, stream) == NULL) {
            report("child fgets", true);
        }

        trim_inplace(in, "\0");
        fprintf(stdout, "[+] %d: received: %s\n", pid, in);
        word = lookup(in);
        if (word != NULL)
            fprintf(stdout, "[+] %d: found translation %s:%s\n", pid, word->name, word->defn);
        else
            fprintf(stdout, "[e] %d: didnt find translation for %s\n", pid, in);

    }
}

/**
 * Funkcja główna programu.
 * Program ma funkcjonalność prostego słownika, tłumaczącego z 5 przykładowych słów z języka angielskiego na polski.
 * Moduł odczytujący komunikaty od użytkownika, i moduł tłumaczących słowa znajdują się w dwóch,
 * oddzielnych procesach systemowych, rozdzielonych za pomocą funkcji fork().
 * Funkcja main wykonuje następujące działania:
 * 1. Tworzy parę połączonych gniazd UNIX, za pomocą funkcji socketpair();
 * 2. Rozwidla proces za pomocą funkcji fork()
 * 3. Ustawia parametry buforowania
 * 4. Dla procesu macierzystego wykonuje funkcję parent(), i przekazuje do niej gniazdo A
 * 5. Dla procesu potomnego, wykonuje funkcję child(), i przekazuje do niej gniazdo B
 *
 * @return
 */
int main (void) {
    int fd[2];
    FILE * restrict stream;
    pid_t pid;

    if (socketpair(AF_UNIX, SOCK_STREAM, 0, fd) != 0) {
        report("socketpair", true);
    }

    pid = fork();
    if (pid == -1) {
        report("fork", true);
    } else if (pid > 0) {                      // parent
        close(fd[1]);
        stream = (FILE * restrict) fdopen(fd[0], "r+b");
        setvbuf(stream, NULL, _IOLBF, MAX_LENGTH);
        if (stream == NULL) {
            report("fdopen", true);
        }

        parent(stream);

    } else {                                     // child
        close(fd[0]);
        stream = (FILE * restrict) fdopen(fd[1], "r+b");
        setvbuf(stream, NULL, _IOLBF, MAX_LENGTH);
        if (stream == NULL) {
            report("fdopen", true);
        }

        child(stream);

    }
}