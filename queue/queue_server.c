/**
 * @file queue_server.c
 * @brief Kod źródłowy implementujący funkcjonalność serwera kolejek POSIX
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <mqueue.h>

#include "./../includes/epoll_local.h"

/**
 * Nazwa kolejki
 */
#define QUEUE_NAME   "/wsh-queue"

/**
 * Uprawnienia do kolejki
 */
#define QUEUE_PERMISSIONS 0666

/**
 * Maksymalna ilość oczekujących, nieodebranych komunikatów w kolejce
 */
#define QUEUE_MAX_MESSAGES 10

/**
 * Maksymalna długość pojedynczego komunikatu w kolejce
 */
#define QUEUE_MESSAGE_SIZE 256

/**
 * Zmienna sterująca; przechowuje inf. czy należy zakończyć działanie programu.
 */
unsigned char quit = 0;

/**
 * Funkcja zgłaszająca błąd i kończąca działanie programu.
 * @param msg Wiadomość błędu
 */
void error (char *msg)
{
    perror (msg);
    exit (EXIT_FAILURE);
}

/**
 * Prosty handler sygnału.
 * Ustawia zmienną globalną quit na 1
 * @param signo Numer sygnału
 * @param siginfo Struktura siginfo_t zawierająca dodatkowe informacje o wywołaniu sygnału
 * @param context
 */
static void signal_handler(int signo, siginfo_t *siginfo, void *context)
{
    quit = 1;
}

/**
 * Funkcja instalująca obsługę przychodzących sygnałów za pomocą sigaction()
 * @param signo Numer sygnału
 * @param handler_function Wskaźnik do funkcji która powinna zostać uruchomiona gdy przychodzi sygnał
 */
static void install_handler(unsigned int signo, void (*handler_function)(int signo, siginfo_t *siginfo, void *context))
{
    struct sigaction handler;
    handler.sa_sigaction = handler_function;
    handler.sa_flags = SA_SIGINFO;
    if (sigaction(signo, &handler, NULL) < 0) {
        error("sigaction");
    }
}

/**
 * Program implementuje funkcjonalność serwera kolejki POSIX, którego zdarzenia są obsługiwane przez epoll
 * @return
 */
int main (void)
{
    struct epoll_event events[32];
    struct mq_attr attr;
    sigset_t sigset;
    mqd_t qfd;

    sigemptyset(&sigset);
    sigaddset(&sigset, SIGINT);
    sigaddset(&sigset, SIGHUP);
    sigaddset(&sigset, SIGTERM);
    sigaddset(&sigset, SIGQUIT);

    install_handler(SIGINT, &signal_handler);
    install_handler(SIGHUP, &signal_handler);
    install_handler(SIGTERM, &signal_handler);
    install_handler(SIGQUIT, &signal_handler);

    attr.mq_flags = 0;
    attr.mq_maxmsg = QUEUE_MAX_MESSAGES;
    attr.mq_msgsize = QUEUE_MESSAGE_SIZE;
    attr.mq_curmsgs = 0;
    if ((qfd = mq_open(QUEUE_NAME, O_RDONLY | O_CREAT, QUEUE_PERMISSIONS, &attr)) == -1) {
        error("mq_open");
    }

    fcntl(qfd, F_SETFL, fcntl(qfd, F_GETFL, 0) | O_NONBLOCK);

    int epfd = epoll_create(1);
    epoll_add(epfd, qfd, EPOLLIN);

    char in_buffer[QUEUE_MESSAGE_SIZE];

    sigprocmask(SIG_BLOCK, &sigset, NULL);

    int nfds;
    for (;;) {
        // printf(".");
        sigprocmask(SIG_UNBLOCK, &sigset, NULL);
        if (quit == 1) {
            break;
        }
        sigprocmask(SIG_BLOCK, &sigset, NULL);
        nfds = epoll_wait(epfd, events, 32, -1);
        if (nfds == -1) {
            error("epoll_wait()");
        }

        for (int i = 0; i < nfds; i++) {
            if (events[i].data.fd == qfd) {
                memset(in_buffer, 0, QUEUE_MESSAGE_SIZE);
                if (mq_receive (qfd, in_buffer, QUEUE_MESSAGE_SIZE, NULL) == -1) {
                    error ("mq_receive failed");
                }

                printf ("message received: %s\n", in_buffer);
            }
        }
    }


    printf("[+] exiting\n");
    int exit_value;
    if (mq_close(qfd) == -1) {
        perror("Client: mq_close");
        exit_value = EXIT_FAILURE;
    } else {
        exit_value = EXIT_SUCCESS;
    }

    if (mq_unlink(QUEUE_NAME) == -1) {
        perror("Client: mq_unlink");
        exit_value = EXIT_FAILURE;
    } else {
        exit_value = (exit_value == EXIT_SUCCESS) ? EXIT_SUCCESS : EXIT_FAILURE;
    }

    exit(exit_value);

}

