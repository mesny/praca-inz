/**
 * @file queue_client_timer.c
 * @brief Plik zawiera kod źródłowy klienta kolejki; implementuje funkcję zegara
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <mqueue.h>

#define QUEUE_NAME   "/wsh-queue"
#define QUEUE_PERMISSIONS 0660
#define QUEUE_MAX_MESSAGES 10
#define QUEUE_MESSAGE_SIZE 256

/**
 * Uchwyt do utworzonego obiektu kolejki systemowej
 */
mqd_t qfd;

/**
 * Numer PID aktualnego procesu
 */
pid_t pid;

/**
 * Zmienna przechowująca liczbę obsłużonych sygnałów
 */
int handled_signals_n;

/**
 * Tablica przechowująca nazwy sygnałów, dostarczona przez system
 */
extern const char * const sys_siglist[];

/**
 * Funkcja wysyła wiadomość na standardowe wyjście i
 * @param msg Wskaźnik do tekstu
 */
void error (char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

/**
 * Funkcja instaluje obsługę przychodzących sygnałów
 * @param signo Numer sygnału
 * @param siginfo Obiekt siginfo przechowujący dodatkowe dane użytkownika wysłane wraz sygnałem
 * @param context
 */
static void signal_handler (int signo, siginfo_t *siginfo, void *context)
{
    handled_signals_n++;

    int exitvalue;
    char message[QUEUE_MESSAGE_SIZE];

    if (signo == SIGALRM) {
        sprintf(message, "client_timer, pid: %d, val:%d", pid, handled_signals_n);
        if (mq_send(qfd, message, strlen(message) + 1, 0) == -1) {
            error("mq_send() error");
        }

    } else if (signo == SIGINT || signo == SIGHUP || signo == SIGQUIT || signo == SIGTERM) {
        if (mq_close(qfd) == -1) {
            perror("Client: mq_close");
            exitvalue = EXIT_FAILURE;
        } else {
            exitvalue = EXIT_SUCCESS;
        }
        exit(exitvalue);
    }

}

/**
 * Funkcja instaluje zegar systemowy czasu rzeczywistego
 */
static void install_timer()
{
    struct itimerval delay;
    int ret;

    delay.it_value.tv_sec = 5;
    delay.it_value.tv_usec = 0;
    delay.it_interval.tv_sec = 10;
    delay.it_interval.tv_usec = 0;
    ret = setitimer(ITIMER_REAL, &delay, NULL);
    if (ret) {
        error("setitimer");
    }
}

/**
 * Funkcja instaluje obsługę przychodzących sygnałów, wg parametrów podanych w argumentach
 * @param signo Numer sygnału
 * @param handler_function Funkcja która powinna obsługiwać sygnał o wskazanym numerze
 */
static void install_handler(unsigned int signo, void (*handler_function)(int signo, siginfo_t *siginfo, void *context))
{
    struct sigaction handler;
    handler.sa_sigaction = handler_function;
    handler.sa_flags = SA_SIGINFO;
    if (sigaction(signo, &handler, NULL) < 0) {
        error("sigaction");
    }
}

/**
 * Funkcja główna programu
 * @return
 */
int main (void)
{
    qfd = (mqd_t) 0;
    handled_signals_n = 0;
    pid = getpid();

    printf("[+] pid: %d\n", getpid());

    install_handler(SIGINT, &signal_handler);
    install_handler(SIGHUP, &signal_handler);
    install_handler(SIGQUIT, &signal_handler);
    install_handler(SIGTERM, &signal_handler);
    install_handler(SIGALRM, &signal_handler);
    install_timer();

    if ((qfd = mq_open(QUEUE_NAME, O_WRONLY)) == -1)
        error("mq_open");

    for (;;)
        pause();

}