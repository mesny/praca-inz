/**
 * @file queue_client_sleep.c
 * @brief Plik zawiera kod programu klienta kolejki, który implementuje funkcję sleep
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <mqueue.h>

/**
 * Stała określająca nazwę kolejki
 */
#define QUEUE_NAME   "/wsh-queue"

/**
 * Stała określająca uprawnienia do kolejki
 */
#define QUEUE_PERMISSIONS 0660

/**
 * stała określająca maksymalną ilość komunikatów oczekujących na odbiór w kolejce.
 */
#define QUEUE_MAX_MESSAGES 10

/**
 * Stała określająca maksymalną wielkość pojedynczego komunikatu w kolejce.
 */
#define QUEUE_MESSAGE_SIZE 256

mqd_t qfd;
unsigned int loop = 1;

/**
 * Funkcja przyjmuje za argument wskaźnik do tekstu, który chcemy przekazać na standardowe wyjście jako komunikat błędu, i kończy działanie programu.
 * @param msg Wskaźnik do tekstu który chcemy wyświetlić użytkownikowi jako komunikat błędu.
 */
void error (char *msg)
{
    perror(msg);
    mq_close(qfd);
    exit(EXIT_FAILURE);
}

/**
 *
 * @param signo Numer sygnału odebranego przez proces
 * @param siginfo Obiekt siginfo
 * @param context
 */
static void signal_handler(int signo, siginfo_t *siginfo, void *context)
{
    loop = 0;
}

/**
 * Funkcja instaluje obsługę przychodzących sygnałów, wg parametrów podanych w argumentach
 * @param signo Numer sygnału
 * @param handler_function Funkcja która powinna obsługiwać sygnał o wskazanym numerze
 */
static void install_handler(unsigned int signo, void (*handler_function)(int signo, siginfo_t *siginfo, void *context))
{
    struct sigaction handler;
    handler.sa_sigaction = handler_function;
    handler.sa_flags = SA_SIGINFO;
    if (sigaction(signo, &handler, NULL) < 0) {
        error("sigaction");
    }
}

/**
 * Funkcja główna programu
 * @param argc Ilość przekazanych argumentów
 * @param argv Wartości argumentów
 * @return
 */
int main (int argc, char *argv[])
{
    unsigned ms;
    if (argc != 2)
        error("Invalid arguments");

    ms = atoi(argv[1]);
    if (ms < 1)
        error("Invalid argument");

    printf("[+] pid: %d\n", getpid());

    install_handler(SIGINT, &signal_handler);
    install_handler(SIGHUP, &signal_handler);
    install_handler(SIGQUIT, &signal_handler);
    install_handler(SIGTERM, &signal_handler);

    if ((qfd = mq_open(QUEUE_NAME, O_WRONLY)) == -1) {
        error("mq_open");
    }

    unsigned us = ms * 1000;
    pid_t pid = getpid();

    unsigned i = 0;
    char message[QUEUE_MESSAGE_SIZE];

    for (;;) {
        if (loop < 1)
            break;

        usleep(us);
        i++;

        memset(message, 0, QUEUE_MESSAGE_SIZE);
        sprintf(message, "client_sleep, pid: %d, val:%d", pid, i);
        if (mq_send(qfd, message, strlen(message) + 1, 0) == -1) {
            perror("mq_send()");
        }
    }

    if (mq_close(qfd) == -1)
        error("mq_close()");

    exit(EXIT_SUCCESS);
}