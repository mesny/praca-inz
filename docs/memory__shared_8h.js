var memory__shared_8h =
[
    [ "memory_req", "structmemory__req.html", "structmemory__req" ],
    [ "memory_res", "structmemory__res.html", "structmemory__res" ],
    [ "memory_inf", "structmemory__inf.html", "structmemory__inf" ],
    [ "memory_cur", "structmemory__cur.html", "structmemory__cur" ],
    [ "memory_create_cur", "memory__shared_8h.html#ac9e9ce73036f6e0b40fee9701b4dbafc", null ],
    [ "memory_create_inf", "memory__shared_8h.html#adb6475a9903a9ea3247b1484f21c54bc", null ],
    [ "memory_create_req", "memory__shared_8h.html#aabdad6eff6031f8e6ced749be92a158f", null ],
    [ "memory_create_res", "memory__shared_8h.html#a19c212e3c528579ca3ec32b0341617e4", null ],
    [ "report_and_exit", "memory__shared_8h.html#a4a1199eefbaa16cc10cd2fbb9bd70198", null ]
];