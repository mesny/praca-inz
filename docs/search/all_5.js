var searchData=
[
  ['inf_0',['inf',['../structmemory__inf.html#aa79cb1f2e15aeba30b1f48f1f755f4e7',1,'memory_inf']]],
  ['install_1',['install',['../translation_8c.html#a6dbecbf954963f1894cf7c6a091ce029',1,'translation.c']]],
  ['install_5fhandler_2',['install_handler',['../queue__client__sleep_8c.html#a54946d7f659c5636a632a7ce39c661bf',1,'install_handler(unsigned int signo, void(*handler_function)(int signo, siginfo_t *siginfo, void *context)):&#160;queue_client_sleep.c'],['../queue__client__timer_8c.html#a54946d7f659c5636a632a7ce39c661bf',1,'install_handler(unsigned int signo, void(*handler_function)(int signo, siginfo_t *siginfo, void *context)):&#160;queue_client_timer.c'],['../queue__server_8c.html#a54946d7f659c5636a632a7ce39c661bf',1,'install_handler(unsigned int signo, void(*handler_function)(int signo, siginfo_t *siginfo, void *context)):&#160;queue_server.c'],['../sigaction_8c.html#a54946d7f659c5636a632a7ce39c661bf',1,'install_handler(unsigned int signo, void(*handler_function)(int signo, siginfo_t *siginfo, void *context)):&#160;sigaction.c']]],
  ['install_5ftimer_3',['install_timer',['../queue__client__timer_8c.html#a5b348705481f8ab78298a57e325a9f8e',1,'install_timer():&#160;queue_client_timer.c'],['../sigaction_8c.html#a5b348705481f8ab78298a57e325a9f8e',1,'install_timer():&#160;sigaction.c']]]
];
