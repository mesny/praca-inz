var searchData=
[
  ['main_0',['main',['../memory__broker_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;memory_broker.c'],['../memory__client_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;memory_client.c'],['../queue__client__sleep_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;queue_client_sleep.c'],['../queue__client__timer_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;queue_client_timer.c'],['../queue__server_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;queue_server.c'],['../sigaction_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;sigaction.c'],['../signal_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;signal.c'],['../sigqueue_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;sigqueue.c'],['../epoll__client_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;epoll_client.c'],['../epoll__server_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;epoll_server.c'],['../communication_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;communication.c'],['../translation_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;translation.c']]],
  ['memory_5fbroker_2ec_1',['memory_broker.c',['../memory__broker_8c.html',1,'']]],
  ['memory_5fclient_2ec_2',['memory_client.c',['../memory__client_8c.html',1,'']]],
  ['memory_5fcreate_5fcur_3',['memory_create_cur',['../memory__shared_8h.html#ac9e9ce73036f6e0b40fee9701b4dbafc',1,'memory_shared.h']]],
  ['memory_5fcreate_5finf_4',['memory_create_inf',['../memory__shared_8h.html#adb6475a9903a9ea3247b1484f21c54bc',1,'memory_shared.h']]],
  ['memory_5fcreate_5freq_5',['memory_create_req',['../memory__shared_8h.html#aabdad6eff6031f8e6ced749be92a158f',1,'memory_shared.h']]],
  ['memory_5fcreate_5fres_6',['memory_create_res',['../memory__shared_8h.html#a19c212e3c528579ca3ec32b0341617e4',1,'memory_shared.h']]],
  ['memory_5fcur_7',['memory_cur',['../structmemory__cur.html',1,'']]],
  ['memory_5finf_8',['memory_inf',['../structmemory__inf.html',1,'']]],
  ['memory_5freq_9',['memory_req',['../structmemory__req.html',1,'']]],
  ['memory_5fres_10',['memory_res',['../structmemory__res.html',1,'']]],
  ['memory_5fshared_2eh_11',['memory_shared.h',['../memory__shared_8h.html',1,'']]]
];
