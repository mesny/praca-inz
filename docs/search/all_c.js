var searchData=
[
  ['sigaction_2ec_0',['sigaction.c',['../sigaction_8c.html',1,'']]],
  ['signal_2ec_1',['signal.c',['../signal_8c.html',1,'']]],
  ['signal_5fattach_2',['signal_attach',['../signal__local_8h.html#a3832c71193b291af2c1321d6ff208b69',1,'signal_local.h']]],
  ['signal_5fhandler_3',['signal_handler',['../queue__client__sleep_8c.html#ae0ed2ebdb40444ba4253b4b7d32ae7cf',1,'signal_handler(int signo, siginfo_t *siginfo, void *context):&#160;queue_client_sleep.c'],['../queue__client__timer_8c.html#ae0ed2ebdb40444ba4253b4b7d32ae7cf',1,'signal_handler(int signo, siginfo_t *siginfo, void *context):&#160;queue_client_timer.c'],['../queue__server_8c.html#ae0ed2ebdb40444ba4253b4b7d32ae7cf',1,'signal_handler(int signo, siginfo_t *siginfo, void *context):&#160;queue_server.c'],['../sigaction_8c.html#ae0ed2ebdb40444ba4253b4b7d32ae7cf',1,'signal_handler(int signo, siginfo_t *siginfo, void *context):&#160;sigaction.c']]],
  ['signal_5fhandler_5fsigint_4',['signal_handler_sigint',['../signal_8c.html#acdded76481cb324868c25e1fcc9a2e9c',1,'signal.c']]],
  ['signal_5fhandler_5fsigquit_5',['signal_handler_sigquit',['../signal_8c.html#ab4acc367da7a2270c7b0c61f7cc3a538',1,'signal.c']]],
  ['signal_5fhandler_5fsigterm_6',['signal_handler_sigterm',['../signal_8c.html#a00679f2a0d39d2a64c07117973b80296',1,'signal.c']]],
  ['signal_5fhandler_5fsigusr_7',['signal_handler_sigusr',['../signal_8c.html#a5fb06eec064eb1b7d34549f7ec31b274',1,'signal.c']]],
  ['signal_5flocal_2eh_8',['signal_local.h',['../signal__local_8h.html',1,'']]],
  ['signal_5fmask_5fcreate_9',['signal_mask_create',['../signal__local_8h.html#a752ba0f418f23ab1b36b43012cbfd087',1,'signal_local.h']]],
  ['signal_5fnotify_10',['signal_notify',['../signal__local_8h.html#af0c95fa2869f51e45acfd9a6317c8dde',1,'signal_local.h']]],
  ['signal_5fread_11',['signal_read',['../signal__local_8h.html#a530012d735a9abbbd43c33ea9b2eae45',1,'signal_local.h']]],
  ['sigqueue_2ec_12',['sigqueue.c',['../sigqueue_8c.html',1,'']]],
  ['sys_5fsiglist_13',['sys_siglist',['../signal__local_8h.html#a0a766bdd73f5cb21fff4659e00f38389',1,'sys_siglist():&#160;signal_local.h'],['../memory__broker_8c.html#a0a766bdd73f5cb21fff4659e00f38389',1,'sys_siglist():&#160;memory_broker.c'],['../queue__client__timer_8c.html#a0a766bdd73f5cb21fff4659e00f38389',1,'sys_siglist():&#160;queue_client_timer.c'],['../sigaction_8c.html#a0a766bdd73f5cb21fff4659e00f38389',1,'sys_siglist():&#160;sigaction.c'],['../signal_8c.html#a0a766bdd73f5cb21fff4659e00f38389',1,'sys_siglist():&#160;signal.c'],['../epoll__client_8c.html#a0a766bdd73f5cb21fff4659e00f38389',1,'sys_siglist():&#160;epoll_client.c']]]
];
