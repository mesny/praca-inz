var searchData=
[
  ['readme_2emd_0',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['report_1',['report',['../sigaction_8c.html#a811b23be7189de588c1f8788b37a5b5a',1,'report(const char *message, bool terminate):&#160;sigaction.c'],['../sigqueue_8c.html#a811b23be7189de588c1f8788b37a5b5a',1,'report(const char *message, bool terminate):&#160;sigqueue.c'],['../communication_8c.html#a811b23be7189de588c1f8788b37a5b5a',1,'report(const char *message, bool terminate):&#160;communication.c'],['../translation_8c.html#a811b23be7189de588c1f8788b37a5b5a',1,'report(const char *message, bool terminate):&#160;translation.c']]],
  ['report_5fand_5fexit_2',['report_and_exit',['../memory__shared_8h.html#a4a1199eefbaa16cc10cd2fbb9bd70198',1,'memory_shared.h']]],
  ['req_3',['req',['../structmemory__req.html#a2d3482dcc74c8fcf0e2b56a016882eb3',1,'memory_req']]],
  ['res_4',['res',['../structmemory__res.html#a50f176f7d9573e5da4ed60620de76c2b',1,'memory_res']]]
];
